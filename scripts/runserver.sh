#!/bin/bash

[ "${#}" -lt 1 ] && { echo "[usage] $0 <htmlcontents.tar.gz>" 1>&2; exit 1; }
htmlarchive="${1}"
base=$(readlink -f "${0}")
base="${base%/*/*}"
extractpath=/tmp/$(basename "${base}")-"${$}"
mkdir -p "${extractpath}"
tar xvzf "${htmlarchive}" -C "${extractpath}"
python3 -m http.server --directory "${extractpath}" --bind 0.0.0.0 1313
rm -fr "${extractpath}"
