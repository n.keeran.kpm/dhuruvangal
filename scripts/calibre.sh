#!/bin/bash

if which ebook-convert; then
    ebook-convert "${@}"
elif which flatpak; then
    flatpak remote-add --user --if-not-exists flathub-user https://dl.flathub.org/repo/flathub.flatpakrepo
    flatpak install --user --assumeyes com.calibre_ebook.calibre
    flatpak run --user --command="ebook-convert" com.calibre_ebook.calibre "${@}"
else
    echo "ebook-convert or flatpak not available" 1>&2
    exit 1
fi
