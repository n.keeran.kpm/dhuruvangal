outdir := $(shell pwd)/dhuruvangal

all: markdown html pdf4 pdf6 epub mobi docx coverimage

genoutdir:
	mkdir -p $(outdir)

markdown: genoutdir
	(cd docs; make markdown; find _build/markdown/ -name '*.md' -printf '%P\n' | xargs tar cvzf $(outdir)/dhuruvangal.md.tar.gz -C _build/markdown)

html: genoutdir
	(cd docs; make html; (find _build/html/ -name '*.html' -printf '%P\n'; echo "_static") | xargs tar cvzf $(outdir)/dhuruvangal.html.tar.gz -C _build/html)

pdf4: genoutdir
	(cd docs; make latexpdf; cp _build/latex/sphinx.pdf $(outdir)/dhuruvangal-a4.pdf)

pdf6: genoutdir
	(cd docs; PSIZE=paperwidth=255.118pt,paperheight=340.157pt,margin=37.52pt make latexpdf; cp _build/latex/sphinx.pdf $(outdir)/dhuruvangal-6inch.pdf)

epub: genoutdir
	(cd docs; make epub; cp _build/epub/sphinx.epub $(outdir)/dhuruvangal.epub)

mobi: epub
	scripts/calibre.sh $(outdir)/dhuruvangal.epub $(outdir)/dhuruvangal.mobi

docx: genoutdir
	(cd docs; make docx; cp _build/docx/sphinx.docx $(outdir)/dhuruvangal.docx)

coverimage: genoutdir
	(cd docs; cp _static/cover.png $(outdir)/cover.png)

clean:
	make -C docs clean
	rm -fr $(outdir)

.PHONY: genoutdir markdown html pdf4 pdf6 epub mobi docx clean
