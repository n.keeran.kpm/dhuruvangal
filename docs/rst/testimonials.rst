.. title:: அணிந்துரை

**********
அணிந்துரை
**********

.. toctree::
   :caption: அணிந்துரை
   :name: testimonials
   :titlesonly:

   testimonials/parameshwar.rst
   testimonials/muthuramalingam.rst
   testimonials/shrini.rst
