.. title:: ஒன் ஆப் அஸ்

***********
ஒன் ஆப் அஸ்
***********

வழக்கம் போல் கார்த்திகா அன்று காலையிலேயே மதன் கியூபிக்கலுக்கு வந்திருந்தாள். 'நேத்து நீங்க ப்ராசஸ் பத்தி விளக்கிச் சொன்னீங்கல்ல, போய்ப் படிச்சேன். சிஸ்டத்துல என்னென்ன ப்ராசஸ் ரன் ஆகுது, அந்த ப்ராசஸ்கள எப்படிப் பார்க்கிறது, என்ன கமாண்ட் யூஸ் பண்ணனும், எல்லாம் தெரிஞ்சிக்கிட்டு வந்திருக்கேன். சொல்லட்டுமா' கார்த்திகா கேட்க 'ஓக்கே' என்று மதன் கூறினான்.

'முதல்ல ps கமாண்ட், இந்த கமாண்ட் மூலம் நம்ம சிஸ்டத்தில் ரன் ஆகிக்கொண்டிருக்கும் அத்தனை ப்ராசஸ்களையும் பார்க்க முடியும், இந்த கமாண்ட் அப்படியே எந்த ஆப்ஷனும் கொடுக்காம ரன் செஞ்சா கரண்ட் டெர்மினலோட அசோசியேட் ஆகி இருக்கும் ப்ராசஸ் மட்டும் லிஸ்ட் பண்ணும்,

.. code-block:: console

   $ ps
       PID TTY          TIME CMD
     15301 pts/8    00:00:00 bash
     15524 pts/8    00:00:00 ps
   $

இதே கமாண்டுக்கு x ஆப்ஷன் கொடுத்தா, கரண்ட் யூசரோட எல்லா ப்ராசஸ்களையும் காட்டும்

.. code-block:: console

   $ ps x
   
       PID TTY      STAT   TIME COMMAND
       832 ?        Ss     0:00 /usr/lib/systemd/systemd --user
       833 ?        S      0:00 (sd-pam)
       843 ?        Sl     0:00 /usr/bin/gnome-keyring-daemon --daemonize --login
       847 tty2     Ssl+   0:00 /usr/lib/gdm-wayland-session /usr/bin/gnome-session
       849 ?        Ss     0:03 /usr/bin/dbus-daemon --session --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
       851 tty2     Sl+    0:00 /usr/lib/gnome-session-binary
       877 ?        Ssl    0:00 /usr/lib/gnome-session-ctl --monitor
       879 ?        Ssl    0:01 /usr/lib/gnome-session-binary --systemd-service --session=gnome
       894 ?        Rsl   32:46 /usr/bin/gnome-shell
       905 ?        Ssl    0:00 /usr/lib/gvfsd
       910 ?        Sl     0:00 /usr/lib/gvfsd-fuse /run/user/1000/gvfs -f
       917 ?        Ssl    0:00 /usr/lib/at-spi-bus-launcher
       923 ?        S      0:00 /usr/bin/dbus-daemon --config-file=/usr/share/defaults/at-spi2/accessibility.conf --nofork --print-address 3
       951 ?        Ssl    0:00 /usr/lib/xdg-permission-store
       957 ?        Sl     0:00 /usr/lib/gnome-shell-calendar-server
       964 ?        Ssl    0:00 /usr/lib/evolution-source-registry
       965 ?        S<sl  27:30 /usr/bin/pulseaudio --daemonize=no --log-target=journal
       971 ?        Sl     0:00 /usr/lib/goa-daemon
       972 ?        Ssl    0:00 /usr/lib/gvfs-udisks2-volume-monitor
       978 ?        Ssl    0:00 /usr/lib/evolution-calendar-factory
      1002 ?        Sl     0:00 /usr/lib/pulse/gsettings-helper
      1027 ?        Sl     0:00 /usr/lib/goa-identity-service
      1028 ?        Ssl    0:00 /usr/lib/gvfs-mtp-volume-monitor
      1038 ?        Ssl    0:00 /usr/lib/dconf-service
      1039 ?        Ssl    0:00 /usr/lib/gvfs-gphoto2-volume-monitor
      1046 ?        Ssl    0:00 /usr/lib/evolution-addressbook-factory
      1050 ?        Ssl    0:00 /usr/lib/gvfs-goa-volume-monitor
      1056 ?        Ssl    0:01 /usr/lib/gvfs-afc-volume-monitor
      1071 ?        Sl     0:00 /usr/bin/gjs /usr/share/gnome-shell/org.gnome.Shell.Notifications
      1073 ?        Sl     0:00 /usr/lib/at-spi2-registryd --use-gnome-session
      1084 ?        Ssl    0:00 /usr/lib/gsd-a11y-settings
      1085 ?        Ssl    0:00 /usr/lib/gsd-color
      1087 ?        Ssl    0:00 /usr/lib/gsd-datetime
      1089 ?        Ssl    0:01 /usr/lib/gsd-housekeeping
      1090 ?        Ssl    0:00 /usr/lib/gsd-keyboard
      1091 ?        Ssl    0:01 /usr/lib/gsd-media-keys
      1092 ?        Ssl    0:00 /usr/lib/gsd-power
      1093 ?        Ssl    0:00 /usr/lib/gsd-print-notifications
      1096 ?        Ssl    0:00 /usr/lib/gsd-rfkill
      1097 ?        Ssl    0:00 /usr/lib/gsd-screensaver-proxy
      1098 ?        Ssl    0:00 /usr/lib/gsd-sharing
      1099 ?        Ssl    0:00 /usr/lib/gsd-smartcard
      1103 ?        Ssl    0:00 /usr/lib/gsd-sound
      1107 ?        Ssl    0:00 /usr/lib/gsd-usb-protection
      1113 ?        Ssl    0:00 /usr/lib/gsd-wacom
      1118 ?        Sl     0:00 /usr/lib/evolution-data-server/evolution-alarm-notify
      1137 ?        Sl     0:00 /usr/lib/gsd-disk-utility-notify
      1178 ?        Sl     0:00 /usr/bin/gjs /usr/share/gnome-shell/org.gnome.ScreenSaver
      1181 ?        Sl     0:00 /usr/lib/gsd-printer
      1264 ?        Ssl    1:33 /usr/lib/gnome-terminal-server
      1314 ?        Sl   106:13 /usr/lib/firefox/firefox
      1364 ?        Sl     0:00 /usr/lib/firefox/firefox -contentproc -parentBuildID 20210923165649 -prefsLen 1 -prefMapSize 246833 -appdir /usr/lib/firefox/browser 1314 true socket
      1434 ?        Sl     2:20 /usr/lib/firefox/firefox -contentproc -childID 2 -isForBrowser -prefsLen 5043 -prefMapSize 246833 -jsInit 285716 -parentBuildID 20210923165649 -appdir /usr/lib/firefox/browser 1314 true tab
      1501 ?        Sl    39:07 /usr/lib/firefox/firefox -contentproc -childID 3 -isForBrowser -prefsLen 5838 -prefMapSize 246833 -jsInit 285716 -parentBuildID 20210923165649 -appdir /usr/lib/firefox/browser 1314 true tab
      1532 ?        Sl    37:49 /usr/lib/firefox/firefox -contentproc -childID 4 -isForBrowser -prefsLen 7235 -prefMapSize 246833 -jsInit 285716 -parentBuildID 20210923165649 -appdir /usr/lib/firefox/browser 1314 true tab
      1586 ?        Sl     0:00 /usr/lib/firefox/firefox -contentproc -parentBuildID 20210923165649 -prefsLen 8441 -prefMapSize 246833 -appdir /usr/lib/firefox/browser 1314 true rdd
      1661 ?        Sl     0:29 /usr/lib/firefox/firefox -contentproc -childID 6 -isForBrowser -prefsLen 8515 -prefMapSize 246833 -jsInit 285716 -parentBuildID 20210923165649 -appdir /usr/lib/firefox/browser 1314 true tab
      1736 ?        Ssl    0:00 /usr/lib/gvfsd-metadata
      2918 ?        S      0:00 /usr/bin/ssh-agent -D -a /run/user/1000/keyring/.ssh
      2922 ?        Ss     0:00 /usr/lib/systemd/systemd --user
      2923 ?        S      0:00 (sd-pam)
      3060 ?        Sl     1:06 /usr/bin/Xwayland :0 -rootless -noreset -accessx -core -auth /run/user/1000/.mutter-Xwaylandauth.Z0BOC1 -listenfd 4 -listenfd 5 -displayfd 6 -initfd 7
      3064 ?        Sl     1:55 ibus-daemon --panel disable -r --xim
      3066 ?        Ssl    0:01 /usr/lib/gsd-xsettings
      3072 ?        Sl     0:00 /usr/lib/ibus/ibus-dconf
      3073 ?        Sl     0:23 /usr/lib/ibus/ibus-extension-gtk3
      3075 ?        Sl     0:23 /usr/lib/ibus/ibus-x11 --kill-daemon
      3081 ?        Sl     0:04 /usr/lib/ibus/ibus-portal
      3103 ?        Sl     0:21 /usr/lib/ibus/ibus-engine-simple
      3142 ?        Sl     0:13 /usr/lib/ibus/ibus-engine-m17n --ibus
      3202 ?        Sl     0:00 /usr/bin/gnome-calendar --gapplication-service
      3208 ?        Sl     0:11 /usr/bin/gnome-software --gapplication-service
      3244 ?        Sl     0:00 /usr/lib/gvfsd-trash --spawner :1.16 /org/gtk/gvfs/exec_spaw/0
      3285 ?        SNsl   0:01 /usr/lib/tracker-miner-fs-3
     12166 ?        Sl    39:01 /usr/lib/firefox/firefox -contentproc -childID 33 -isForBrowser -prefsLen 9824 -prefMapSize 246833 -jsInit 285716 -parentBuildID 20210923165649 -appdir /usr/lib/firefox/browser 1314 true tab
     12972 ?        Sl     0:00 /usr/lib/firefox/firefox -contentproc -childID 35 -isForBrowser -prefsLen 9824 -prefMapSize 246833 -jsInit 285716 -parentBuildID 20210923165649 -appdir /usr/lib/firefox/browser 1314 true tab
     15301 pts/8    Ss     0:00 -bash
     15555 pts/8    R+     0:00 ps x
   $

இதே கமாண்டுக்கு a ஆப்ஷன் கொடுத்தால் சிஸ்டத்தில் இருக்கும் டெர்மினல்சோட அசோசியேட் ஆகி இருக்கும் அத்தனை ப்ராசஸ்களையும் காட்டும்

.. code-block:: console

   $ ps a
       PID TTY      STAT   TIME COMMAND
       847 tty2     Ssl+   0:00 /usr/lib/gdm-wayland-session /usr/bin/gnome-session
       851 tty2     Sl+    0:00 /usr/lib/gnome-session-binary
      4865 pts/0    Ss     0:00 -bash
     16630 pts/0    R+     0:00 ps a
   $

இப்போ ax ரெண்டு ஆப்ஷனையும் சேர்த்துக் கொடுத்தா சிஸ்டத்துல கரண்டா ரன் ஆகும் அத்தனை ப்ராசஸ்களையும் காட்டும்

.. code-block:: console

   $ ps ax
       PID TTY      STAT   TIME COMMAND
         1 ?        Ss     0:01 /sbin/init
         2 ?        S      0:00 [kthreadd]
         3 ?        I<     0:00 [rcu_gp]
         4 ?        I<     0:00 [rcu_par_gp]
         6 ?        I<     0:00 [kworker/0:0H-events_highpri]
         8 ?        I<     0:00 [mm_percpu_wq]
        10 ?        S      0:00 [rcu_tasks_kthre]
        11 ?        S      0:00 [rcu_tasks_rude_]
        12 ?        S      0:00 [rcu_tasks_trace]
        13 ?        S      0:05 [ksoftirqd/0]
        14 ?        I      0:16 [rcu_preempt]
        15 ?        S      0:00 [rcub/0]
        16 ?        S      0:00 [rcuc/0]
        17 ?        S      0:00 [migration/0]
        18 ?        S      0:00 [idle_inject/0]
        20 ?        S      0:00 [cpuhp/0]
        21 ?        S      0:00 [cpuhp/1]
        22 ?        S      0:00 [idle_inject/1]
        23 ?        S      0:00 [migration/1]
        24 ?        S      0:00 [rcuc/1]
        25 ?        S      0:02 [ksoftirqd/1]
        27 ?        I<     0:00 [kworker/1:0H-events_highpri]
        28 ?        S      0:00 [cpuhp/2]
        29 ?        S      0:00 [idle_inject/2]
        30 ?        S      0:00 [migration/2]
        31 ?        S      0:00 [rcuc/2]
        32 ?        S      0:02 [ksoftirqd/2]
        34 ?        I<     0:00 [kworker/2:0H-kblockd]
        35 ?        S      0:00 [cpuhp/3]
        36 ?        S      0:00 [idle_inject/3]
        37 ?        S      0:00 [migration/3]
        38 ?        S      0:00 [rcuc/3]
        39 ?        S      0:01 [ksoftirqd/3]
        41 ?        I<     0:00 [kworker/3:0H-events_highpri]
        42 ?        S      0:00 [kdevtmpfs]
        43 ?        I<     0:00 [netns]
        44 ?        I<     0:00 [inet_frag_wq]
        45 ?        S      0:00 [kauditd]
        46 ?        S      0:00 [khungtaskd]
        47 ?        S      0:00 [oom_reaper]
        48 ?        I<     0:00 [writeback]
        49 ?        S      0:04 [kcompactd0]
        50 ?        SN     0:00 [ksmd]
        51 ?        SN     0:00 [khugepaged]
        71 ?        I<     0:00 [kintegrityd]
        72 ?        I<     0:00 [kblockd]
        73 ?        I<     0:00 [blkcg_punt_bio]
        74 ?        I<     0:00 [ata_sff]
        75 ?        I<     0:00 [edac-poller]
        76 ?        I<     0:00 [devfreq_wq]
        77 ?        S      0:00 [watchdogd]
        79 ?        I<     0:04 [kworker/1:1H-events_highpri]
        80 ?        S      0:09 [kswapd0]
        83 ?        I<     0:00 [kthrotld]
        85 ?        I<     0:00 [acpi_thermal_pm]
        86 ?        I<     0:00 [nvme-wq]
        87 ?        I<     0:00 [nvme-reset-wq]
        88 ?        I<     0:00 [nvme-delete-wq]
        90 ?        S      0:00 [scsi_eh_0]
        91 ?        I<     0:00 [scsi_tmf_0]
        92 ?        S      0:00 [scsi_eh_1]
        93 ?        I<     0:00 [scsi_tmf_1]
        94 ?        S      0:00 [scsi_eh_2]
        95 ?        I<     0:00 [scsi_tmf_2]
        96 ?        S      0:00 [scsi_eh_3]
        97 ?        I<     0:00 [scsi_tmf_3]
        98 ?        S      0:00 [scsi_eh_4]
        99 ?        I<     0:00 [scsi_tmf_4]
       100 ?        S      0:00 [scsi_eh_5]
       101 ?        I<     0:00 [scsi_tmf_5]
       108 ?        I<     0:00 [kstrp]
       114 ?        I<     0:00 [zswap1]
       115 ?        I<     0:00 [zswap1]
       116 ?        I<     0:00 [zswap-shrink]
       118 ?        I<     0:00 [charger_manager]
       121 ?        I<     0:04 [kworker/2:1H-events_highpri]
       134 ?        I<     0:04 [kworker/0:1H-events_highpri]
       152 ?        I<     0:03 [kworker/3:1H-events_highpri]
       164 ?        I<     0:00 [sdhci]
       170 ?        I<     0:00 [kdmflush]
       182 ?        I<     0:00 [xfsalloc]
       183 ?        I<     0:00 [xfs_mru_cache]
       184 ?        I<     0:00 [xfs-buf/dm-0]
       185 ?        I<     0:00 [xfs-conv/dm-0]
       186 ?        I<     0:00 [xfs-cil/dm-0]
       187 ?        I<     0:00 [xfs-reclaim/dm-]
       188 ?        I<     0:00 [xfs-gc/dm-0]
       189 ?        I<     0:00 [xfs-log/dm-0]
       190 ?        S      0:06 [xfsaild/dm-0]
       226 ?        Ss     0:01 /usr/lib/systemd/systemd-journald
       240 ?        Ss     0:00 /usr/lib/systemd/systemd-udevd
       247 ?        Ssl    0:00 /usr/lib/systemd/systemd-networkd
       275 ?        I<     0:00 [tpm_dev_wq]
       290 ?        I<     0:00 [ktpacpid]
       300 ?        I<     0:00 [wg-crypt-wg4000]
       308 ?        I<     0:00 [cfg80211]
       313 ?        I<     0:00 [cryptd]
       319 ?        Ss     0:03 /usr/lib/systemd/systemd-resolved
       320 ?        Ssl    0:00 /usr/lib/systemd/systemd-timesyncd
       327 ?        S      0:22 [irq/27-iwlwifi]
       344 ?        Ss     0:01 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
       349 ?        Ss     0:00 /usr/lib/iwd/iwd
       354 ?        Ss     0:00 /usr/lib/systemd/systemd-logind
       355 ?        Ss     0:00 /usr/lib/systemd/systemd-machined
       367 ?        S      0:00 [card0-crtc0]
       368 ?        S      0:00 [card0-crtc1]
       496 ?        Ss     0:00 sshd: /usr/bin/sshd -D [listener] 0 of 10-100 startups
       498 ?        Ssl    0:00 /usr/bin/gdm
       504 ?        Ssl    0:00 /usr/lib/accounts-daemon
       509 ?        Ssl    0:00 /usr/lib/polkit-1/polkitd --no-debug
       623 ?        Ssl    0:01 /usr/lib/upowerd
       630 ?        SNsl   0:00 /usr/lib/rtkit-daemon
       758 ?        Ssl    0:00 /usr/lib/colord
       821 ?        Sl     0:00 gdm-session-worker [pam/gdm-password]
       832 ?        Ss     0:00 /usr/lib/systemd/systemd --user
       833 ?        S      0:00 (sd-pam)
       843 ?        Sl     0:00 /usr/bin/gnome-keyring-daemon --daemonize --login
       847 tty2     Ssl+   0:00 /usr/lib/gdm-wayland-session /usr/bin/gnome-session
       849 ?        Ss     0:04 /usr/bin/dbus-daemon --session --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
       851 tty2     Sl+    0:00 /usr/lib/gnome-session-binary
       877 ?        Ssl    0:00 /usr/lib/gnome-session-ctl --monitor
       879 ?        Ssl    0:01 /usr/lib/gnome-session-binary --systemd-service --session=gnome
       894 ?        Ssl   36:32 /usr/bin/gnome-shell
       905 ?        Ssl    0:00 /usr/lib/gvfsd
       910 ?        Sl     0:00 /usr/lib/gvfsd-fuse /run/user/1000/gvfs -f
       917 ?        Ssl    0:00 /usr/lib/at-spi-bus-launcher
       923 ?        S      0:00 /usr/bin/dbus-daemon --config-file=/usr/share/defaults/at-spi2/accessibility.conf --nofork --print-address 3
       951 ?        Ssl    0:00 /usr/lib/xdg-permission-store
       957 ?        Sl     0:00 /usr/lib/gnome-shell-calendar-server
       964 ?        Ssl    0:00 /usr/lib/evolution-source-registry
       965 ?        S<sl  33:42 /usr/bin/pulseaudio --daemonize=no --log-target=journal
       971 ?        Sl     0:00 /usr/lib/goa-daemon
       972 ?        Ssl    0:00 /usr/lib/gvfs-udisks2-volume-monitor
       975 ?        Ssl    0:03 /usr/lib/udisks2/udisksd
       978 ?        Ssl    0:00 /usr/lib/evolution-calendar-factory
      1002 ?        Sl     0:00 /usr/lib/pulse/gsettings-helper
      1027 ?        Sl     0:00 /usr/lib/goa-identity-service
      1028 ?        Ssl    0:00 /usr/lib/gvfs-mtp-volume-monitor
      1038 ?        Ssl    0:00 /usr/lib/dconf-service
      1039 ?        Ssl    0:00 /usr/lib/gvfs-gphoto2-volume-monitor
      1046 ?        Ssl    0:00 /usr/lib/evolution-addressbook-factory
      1050 ?        Ssl    0:00 /usr/lib/gvfs-goa-volume-monitor
      1056 ?        Ssl    0:01 /usr/lib/gvfs-afc-volume-monitor
      1071 ?        Sl     0:00 /usr/bin/gjs /usr/share/gnome-shell/org.gnome.Shell.Notifications
      1073 ?        Sl     0:00 /usr/lib/at-spi2-registryd --use-gnome-session
      1084 ?        Ssl    0:00 /usr/lib/gsd-a11y-settings
      1085 ?        Ssl    0:00 /usr/lib/gsd-color
      1087 ?        Ssl    0:00 /usr/lib/gsd-datetime
      1089 ?        Ssl    0:01 /usr/lib/gsd-housekeeping
      1090 ?        Ssl    0:00 /usr/lib/gsd-keyboard
      1091 ?        Ssl    0:01 /usr/lib/gsd-media-keys
      1092 ?        Ssl    0:00 /usr/lib/gsd-power
      1093 ?        Ssl    0:00 /usr/lib/gsd-print-notifications
      1096 ?        Ssl    0:00 /usr/lib/gsd-rfkill
      1097 ?        Ssl    0:00 /usr/lib/gsd-screensaver-proxy
      1098 ?        Ssl    0:00 /usr/lib/gsd-sharing
      1099 ?        Ssl    0:00 /usr/lib/gsd-smartcard
      1103 ?        Ssl    0:00 /usr/lib/gsd-sound
      1107 ?        Ssl    0:00 /usr/lib/gsd-usb-protection
      1113 ?        Ssl    0:00 /usr/lib/gsd-wacom
      1118 ?        Sl     0:00 /usr/lib/evolution-data-server/evolution-alarm-notify
      1137 ?        Sl     0:00 /usr/lib/gsd-disk-utility-notify
      1178 ?        Sl     0:00 /usr/bin/gjs /usr/share/gnome-shell/org.gnome.ScreenSaver
      1181 ?        Sl     0:00 /usr/lib/gsd-printer
      1264 ?        Ssl    1:59 /usr/lib/gnome-terminal-server
      1314 ?        Sl   115:38 /usr/lib/firefox/firefox
      1364 ?        Sl     0:00 /usr/lib/firefox/firefox -contentproc -parentBuildID 20210923165649 -prefsLen 1 -prefMapSize 246833 -appdir /usr/lib/firefox/browser 1314 true socket
      1434 ?        Sl     2:32 /usr/lib/firefox/firefox -contentproc -childID 2 -isForBrowser -prefsLen 5043 -prefMapSize 246833 -jsInit 285716 -parentBuildID 20210923165649 -appdir /usr/lib/firefox/browser 1314 true tab
      1501 ?        Sl    49:54 /usr/lib/firefox/firefox -contentproc -childID 3 -isForBrowser -prefsLen 5838 -prefMapSize 246833 -jsInit 285716 -parentBuildID 20210923165649 -appdir /usr/lib/firefox/browser 1314 true tab
      1532 ?        Sl    40:14 /usr/lib/firefox/firefox -contentproc -childID 4 -isForBrowser -prefsLen 7235 -prefMapSize 246833 -jsInit 285716 -parentBuildID 20210923165649 -appdir /usr/lib/firefox/browser 1314 true tab
      1586 ?        Sl     0:00 /usr/lib/firefox/firefox -contentproc -parentBuildID 20210923165649 -prefsLen 8441 -prefMapSize 246833 -appdir /usr/lib/firefox/browser 1314 true rdd
      1661 ?        Sl     0:33 /usr/lib/firefox/firefox -contentproc -childID 6 -isForBrowser -prefsLen 8515 -prefMapSize 246833 -jsInit 285716 -parentBuildID 20210923165649 -appdir /usr/lib/firefox/browser 1314 true tab
      1736 ?        Ssl    0:00 /usr/lib/gvfsd-metadata
      2840 ?        Ss     0:00 /usr/lib/systemd/systemd
      2867 ?        Ss     0:00 /usr/lib/systemd/systemd-journald
      2874 ?        Ss     0:00 /usr/lib/systemd/systemd-networkd
      2879 ?        Ss     0:00 /usr/lib/systemd/systemd-resolved
      2884 ?        Ss     0:00 /usr/bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation --syslog-only
      2885 ?        Ss     0:00 /usr/lib/systemd/systemd-logind
      2918 ?        S      0:00 /usr/bin/ssh-agent -D -a /run/user/1000/keyring/.ssh
      2922 ?        Ss     0:00 /usr/lib/systemd/systemd --user
      3060 ?        Sl     1:09 /usr/bin/Xwayland :0 -rootless -noreset -accessx -core -auth /run/user/1000/.mutter-Xwaylandauth.Z0BOC1 -listenfd 4 -listenfd 5 -displayfd 6 -initfd 7
      3064 ?        Sl     2:30 ibus-daemon --panel disable -r --xim
      3066 ?        Ssl    0:01 /usr/lib/gsd-xsettings
      3072 ?        Sl     0:00 /usr/lib/ibus/ibus-dconf
      3073 ?        Sl     0:26 /usr/lib/ibus/ibus-extension-gtk3
      3075 ?        Sl     0:23 /usr/lib/ibus/ibus-x11 --kill-daemon
      3081 ?        Sl     0:05 /usr/lib/ibus/ibus-portal
      3103 ?        Sl     0:32 /usr/lib/ibus/ibus-engine-simple
      3142 ?        Sl     0:14 /usr/lib/ibus/ibus-engine-m17n --ibus
      3202 ?        Sl     0:00 /usr/bin/gnome-calendar --gapplication-service
      3208 ?        Sl     0:11 /usr/bin/gnome-software --gapplication-service
      3244 ?        Sl     0:00 /usr/lib/gvfsd-trash --spawner :1.16 /org/gtk/gvfs/exec_spaw/0
      3285 ?        SNsl   0:01 /usr/lib/tracker-miner-fs-3
     12166 ?        Sl    41:21 /usr/lib/firefox/firefox -contentproc -childID 33 -isForBrowser -prefsLen 9824 -prefMapSize 246833 -jsInit 285716 -parentBuildID 20210923165649 -appdir /usr/lib/firefox/browser 1314 true tab
     17736 pts/4    R+     0:00 ps ax
   $

நான் சொன்னதெல்லாம் கரெக்டா?' கார்த்திகா கேட்க மதன் ஆம் என்பதுபோல் தலை ஆட்டினான்.

'அடுத்து top கமாண்ட், இது ஒரு TUI (Terminal User Interface) வகை கமாண்ட்' கார்த்திகா சொல்லும்போது மதன் குறுக்கிட்டான் 'அப்படின்னா?' மதன் கேட்க 'அதாவது இந்த TUI வகையான அப்ளிகேஷன் GUI (Graphical User Interface) அப்ளிகேஷன் மாதிரியே மெனு, விண்டோ எல்லாம் இருக்கும், ஆனா எல்லாமே டெர்மினல்லையே இருக்கும்' கார்த்திகா விளக்கமளித்தாள். 'அப்ப GUI அப்படின்னா என்ன?' மதன் கேட்க 'உங்களுக்குத் தெரியாதா? விண்டோஸ் யூஸ் பண்ணதே இல்ல? அதுல மெனு விண்டோ எல்லாம் பார்த்ததே இல்ல? சும்மா விளையாடாதீங்க, வேணும்னா கூகுள்ல சர்ச் பண்ணித் தெரிஞ்சிக்கோங்க' கார்த்திகா செல்லமாக கோபித்துக் கொண்டாள். 'top கமாண்டுக்கு வருவோம். இந்த கமாண்ட் சிஸ்டம் எவ்வளவு சிபியு யூஸ் பண்ணிக்கிட்டு இருக்கு, எவ்வளவு மெமரி யூஸ் பண்ணிக்கிட்டு இருக்கு, எந்த ப்ராசஸ் அதிகமா சிபியு யூஸ் பண்ணுது, எந்த ப்ராசஸ் அதிகமா மெமரி யூஸ் பண்ணுது இப்படி எல்லா டீட்டெய்ல்ஸ்சும் காட்டும், கரெக்டா?' கார்த்திகா கேட்க 'கரெக்ட், அது மட்டும் இல்லாம top கமாண்ட் மூலம் ப்ராசஸ்கள் பற்றிய பல விஷயங்கள நாம தெரிஞ்சிக்கலாம், ப்ராசஸ்கல கன்ட்ரோலும் பண்ணலாம்' மதன் விவரித்தான்.

.. code-block:: console

   $ top
   $

'மெமரி சொன்னவுடனே எனக்கு இன்னொரு கமாண்ட் சொல்லத் தோனுது, free கமாண்ட், இந்த கமாண்ட் மூலமா நம்ம சிஸ்டத்துல எவ்வளவு மெமரி இருக்குன்னு தெரிஞ்சிக்கலாம்' கார்த்திகா முடிக்க 'free கமாண்டோட எந்த பீல்ட பார்த்து கரண்ட் சிஸ்டத்தோட மெமரி யூசேஜ தெரிஞ்சிப்பீங்க?' மதன் கேட்க 'இத நீங்க கேட்பீங்கன்னு எதிர்பார்த்தேன், free கமாண்ட் அவுட்புட்ல Available பீல்ட்தான் சிஸ்டத்துல எவ்வளவு மெமரி மிச்சம் இருக்குன்னு காட்டும்' கார்த்திகா சரியாகக் கூறினாள் அதை மதனும் சரி எனத் தலை ஆட்டினான்.

.. code-block:: console

   $ free
                  total        used        free      shared  buff/cache   available
   Mem:         8026564     4231316     2090600      502472     1704648     3010980
   Swap:              0           0           0
   $

'மெமரி எவ்வளவு காலியா இருக்குன்னு வேற வழியிலயும் பார்க்கலாம்' மதன் சொல்லும்போதே 'ஆமாம் top கமாண்டும் எவ்வளவு மெமரி காலியா இருக்குன்னு avail mem பீல்டுல காட்டும்' கார்த்திகா கூற 'ஆமாம், அது மட்டும் இல்லாம, இந்த கமாண்டும் அவைளபிள் மெமரி காட்டும்

.. code-block:: console

   $ cat /proc/meminfo
   MemTotal:        8026564 kB
   MemFree:         2075944 kB
   MemAvailable:    2996384 kB
   Buffers:              36 kB
   Cached:          1571036 kB
   SwapCached:            0 kB
   Active:           645336 kB
   Inactive:        4451160 kB
   Active(anon):       6496 kB
   Inactive(anon):  4016464 kB
   Active(file):     638840 kB
   Inactive(file):   434696 kB
   Unevictable:      423684 kB
   Mlocked:               0 kB
   SwapTotal:             0 kB
   SwapFree:              0 kB
   Dirty:               360 kB
   Writeback:             0 kB
   AnonPages:       3949128 kB
   Mapped:           487424 kB
   Shmem:            503880 kB
   KReclaimable:     135084 kB
   Slab:             226396 kB
   SReclaimable:     135084 kB
   SUnreclaim:        91312 kB
   KernelStack:       15376 kB
   PageTables:        46944 kB
   NFS_Unstable:          0 kB
   Bounce:                0 kB
   WritebackTmp:          0 kB
   CommitLimit:     4013280 kB
   Committed_AS:   11759676 kB
   VmallocTotal:   34359738367 kB
   VmallocUsed:       40268 kB
   VmallocChunk:          0 kB
   Percpu:             6464 kB
   HardwareCorrupted:     0 kB
   AnonHugePages:         0 kB
   ShmemHugePages:        0 kB
   ShmemPmdMapped:        0 kB
   FileHugePages:         0 kB
   FilePmdMapped:         0 kB
   CmaTotal:              0 kB
   CmaFree:               0 kB
   HugePages_Total:       0
   HugePages_Free:        0
   HugePages_Rsvd:        0
   HugePages_Surp:        0
   Hugepagesize:       2048 kB
   Hugetlb:               0 kB
   DirectMap4k:      376448 kB
   DirectMap2M:     7897088 kB
   $

இந்த அவுட்புட்ல MemAvailable லைன் தான் கரண்ட் சிஸ்டத்தில் காலியா இருக்கும் மெமரி' மதன் கூற ' இந்த /proc பைல் சிஸ்டத்துல ப்ராசஸ் பத்தின எல்லா டீட்டெயிஸ்சும் இருக்கும் போல' கார்த்திகா கேட்க ஆமாம் என்பதுபோல் மதன் தலை ஆட்டினான்.

'ஒரு முக்கியமான கமாண்ட விட்டுட்டீங்க, kill கமாண்ட பத்தி படிச்சீங்களா?' மதன் கேட்க, 'ஆமாம், அதச் சொல்ல மறந்துட்டேன், kill கமாண்ட் ஒரு ப்ராசஸ்க்குப் பலவிதமான சிக்னல்ஸ் அனுப்பப் பயன்படுத்தப்படும் கமாண்ட்' கார்த்திகா கூறினாள்

.. code-block:: console

   $ kill 965

'இந்த 965 அப்படின்ற pid என்னோட கரண்ட் ஸிஸ்டத்துல ரன் ஆகும் pulseaudio அப்படின்ற ப்ராசஸ்சோட pid, இப்ப நான் kill கமாண்டுக்கு இந்த 965 pid கொடுத்தா pulseaudio ப்ராசஸ் டெர்மினேட் ஆகிடும், அதுக்கு அப்புறம் என் ஸிஸ்டத்துல ஆடியோ கேட்காது' கார்த்திகா விளக்கினாள், 'அது மட்டும் இல்லாம kill கமாண்டுக்கு -L ஆப்ஷன் கொடுத்தா ஒரு ப்ராசஸ்சுக்கு எத்தனை விதமான சிக்னல்ஸ் அனுப்பலாம்னு காட்டும், சிக்னல்ஸ்சோட சிக்னல் நம்பரையும் காட்டும்' மதன் விவரித்தான்.

.. code-block:: console

   $ kill -L

'இந்த சிக்னல்ஸ்ல முக்கியமான சிக்னல்ஸ் 9) SIGKILL, 11) SIGSEGV, 15) SIGTERM, 18) SIGCONT, 19) SIGSTOP இதுல SIGTERM தான் டீபால்ட் சிக்னல், அதாவது kill கமாண்டுக்கு எந்த சிக்னல் கொடுக்கனும்னு சொல்லலைன்னா அது டீபால்டா 15) SIGTERM ஆ தான் ப்ராசஸ்சுக்கு அனுப்பும், இந்த சிக்னல் வந்தவுடன் அந்த ப்ராசஸ்ல இருக்குற SIGKILL சிக்னலுக்கு உண்டான டீபால்ட் சிக்னல் ஹான்லர் அந்த ப்ராசஸ்ஸ டெர்மினேட் பண்ணிடும். 11) SIGSEGV சிக்னல் ஒரு ப்ராசஸ் ஏதாவது கோக்குமாக்கா வேல செஞ்சதுனா, கர்னல் பொடனியிலேயே ஒன்னு வச்சுப் போய் சாகுன்னு சாகடிக்கப் பயன்படுத்துற சிக்னல், 19) SIGSTOP சிக்னல் பயன்படுத்தி நாம ஒரு ப்ராசஸ்ச ப்ரீஸ் பண்ண முடியும், அதாவது அது எந்த வேலையும் செய்யாது, ஆனா அந்த ப்ராசஸ்ச கர்னல் டெர்மினேட் செய்யாது, அந்த ப்ராசஸ்ஸ 18) SIGCONT கொடுத்து மீண்டும் இயக்க வைக்கலாம்' மதன் கூறினான். '9) SIGKILL பத்தி சொல்லவே இல்லையே?' கார்த்திகா கேட்க, '9) SIGKILL தான் ரொம்ப டேஞ்சரான சிக்னல், ஒரு ப்ராசஸ்ச பாரபட்சமே இல்லாம உடனே சாகடிக்க பயன்படுத்துற சிக்னல், இந்த சிக்னல் ஒரு ப்ராசஸ்சுக்கு அனுப்பப்பட்டா அந்த ப்ராசஸ்ல இருக்குற சிக்னல் ஹாண்ட்லருக்கு அந்த சிக்னல் போகாம கர்னலே அந்த ப்ராசஸ்ச சாகடிச்சுடும், அதனால எப்பவும் இந்த SIGKILL பயன்படுத்தும்போது கவனமா இருக்கனும்' மதன் விரிவாக விவரித்தான். 'சிக்னல்ஸ்ல இவ்வளவு விஷயம் இருக்கா? அமேசிங்' கார்த்திகா ஆச்சரியப்பட்டாள்.

'ஆபீஸ்லையே சூப்பர் சிங்கர் நடத்தி இருக்கீங்க போல' சுரேஷ் மதனின் க்யூப்பிக்களுக்கு தீப்தியுடன் வந்தான், 'கங்கிராட்ஸ் அக்கா அசத்திட்டீங்க' தீப்தி கார்த்திகாவுக்குக் கை கொடுத்துப் பாராட்டினாள். 'பண்றதெல்லாம் பண்ணிட்டு என்னடா பேய் அறஞ்சா மாதிரி முழிக்கிற, உங்க வீடியோ தான் நம்ம கம்பெனி சோஷியல் மீடியா வெப்சைட்ல நம்பர் ஒன், ஓப்பன் பண்ணிப் பாரு' என்று சுரேஷ் கூற மதனும் வெகு நாள் கழித்து அந்த வெப்சைட்டில் லாகின் செய்தான், மதனும் கார்த்திகாவும் பாடிய வீடியோ, அவர்கள் கம்பெனியின் இன்டர்னல் சோஷியல் மீடியா வெப்சைட்டில் முதல் இடத்தில் இருந்தது. இவர்கள் பாடுவதைப் பலரும் அதில் பாராட்டி இருந்தார்கள். மதன், கார்த்திகா, சுரேஷ் மற்றும் தீப்தி நால்வரும் அந்த வீடியோவிற்கு வந்திருக்கும் கமெண்டுகளைப் படிக்க ஆரம்பித்தனர்.

அதிகமான லைக்சும் ரிப்ளைகளும் பெற்ற கமெண்டில் ஒரு பெண் 'நெஞ்சில் உள்ளாடும் ராகம் இது தானா கண்மணி ராதா' வரிகள் பாடும்போது மதன் கார்த்திகாவைப் பார்த்து பாடியதையும் அதற்கு கார்த்திகா முகத்தைத் தன் கையால் மூடிக் கொண்டதையும் குறிப்பிட்டு 'இப் தே ஆர் நாட் மேரிட் ஆர் நாட் கோயிங் டூ மேரி, ஐ அம் கோயிங் டு கில் தெம் போத், சோ மச் லவ் இஸ் இன் த எர்,' என்று கமெண்ட் போட்டிருந்தார். இந்த கமெண்டுக்குப் பலரும் இருவரும் லவ்வர்களாகத்தான் இருக்க வேண்டும் இல்லையெனில் இப்படிப் பாடி இருக்க முடியாது என்று ரிப்ளை கொடுத்திருந்தனர். அடுத்த பாப்புலரான கமெண்டில் ஒருவர் 'போத் சங் கிருஷ்ணா ராதா பேஸ்டு சாங்ஸ். மே த காட்ஸ் கிவ் தர் பிலஸ்ஸிங்ஸ் டு திஸ் கப்புல்' என்று குறிப்பிட்டு இருந்தார்.

'தே ஆர் ஷிப்பிங் அஸ் லைக் எனிதிங்' மதன் கார்த்திகாவைப் பார்த்துக் கூற கார்த்திகா புன்னகைத்தாள். 'தேங்ஸ் பார் ஆல் யுவர் விஷஸ், பட் ப்ளீஸ் ஸ்டாப் ஷிப்பிங் அஸ், ஐ நோ யு ஆர் நாட் கோயிங் டு பிலீவ், பட் வி ஆர் ஜஸ்ட் குட் பிரண்ட்ஸ் டில் நவ். ஐ ஃபியர் திஸ் மே அஃபக்ட் ஹர் ப்யூச்சர் அண்ட் ஐ ஃபீல் கில்டி அபௌட் தட்' மதன் அந்த முதல் கமெண்டில் ரிப்ளை போட்டான். இதைப் பக்கத்தில் இருந்து பார்த்துக்கொண்டிருந்த கார்த்திகா 'நீங்க பிரண்டுன்னு சொன்னவுடனே நம்பிடப்போறாங்களா?' என்று மதனைப் பார்த்துக் கேட்க 'ஸ்டில், தே நீட் டு நோ ரைட்? பின்னாடி பிரச்சனை ஆகும் கார்த்திகா?' மதனும் கார்த்திகாவைப் பார்த்துக் கூற 'நானே அத பெருசா எடுத்துக்கல, நீங்க ஏன் வீணாக் கவலைப்படுறீங்க?' கார்த்திகா கேட்க 'பிரச்சனை உனக்கு மட்டும் இல்லம்மா, நாளைக்கு என் பொண்டாட்டி இதப் பார்த்தா என்ன நெனப்பா?' மதன் கேட்க 'பொசசிவ் இல்லாத பொண்டாட்டியா இருந்தா இதப் பார்த்து உங்களைக் கிண்டல் பண்ணுவா, பொசசிவான பொண்டாட்டின்னா கல்யாணத்துக்கு அப்புறம் உன்ன தவிர வேற எவளையும் பாக்குறதில்லன்னு சொல்லிப் பத்து சவரன் தங்க செயின் வாங்கிக் கொடுத்து சமாளிச்சுக்கோங்க' கார்த்திகா மதனைப் பார்த்துச் சிரித்தபடிக் கூறினாள்.

மற்றொரு கமெண்டில் ஒருவர் மதனின் மானிட்டரையும் கார்த்திகாவின் லேப்டாப்பையும் வீடியோவில் வரும் இடத்தைச் சுட்டிக்காட்டி 'இஸ் எனி ஒன் சீ வாட் ஐ சீ, தட் டெஸ்க்டாப் லுக் லைக் i3 அண்ட் தட் அதர் லேப்டாப் லுக் லைக் cinnamon' என்று குறிப்பிட்டிருந்தார். அந்த கமெண்டைப் பார்த்ததும் 'எக்ஸாக்ட்லி காம்ரேட், இட்ஸ் i3 ஆன் டாப் ஆப் btw அண்ட் அனதர் ஒன் இன் கார்த்திகாஸ் லேப்டாப்,  தட் இஸ் cinnamon ஆன் டாப் ஆப் Mint' என்று ரிப்ளை கொடுத்தான். அருகில் இருந்த கார்த்திகா அதைப் பார்த்து 'Mint புரியுது அது என்ன i3, cinnamon, btw?' என்று கேட்க 'cinnamon உங்கள் டெஸ்க்டாப் என்விரான்மென்ட், நீங்க லாகின் பண்ண உடனே வர்ற யுஐ (UI) ஸ்கிரீன், லினக்ஸ்ல பல டெஸ்க்டாப் என்விரான்மென்ட் இருக்கு, i3 என்னோட டெஸ்க்டாப் என்விரான்மென்ட், வெல், i3 இஸ் ஆக்சுவலி எ விண்டோ மேனேஜர்' என்று கூற 'அப்ப btw? Mint மாதிரி லினக்ஸ் டிஸ்ட்ரோவோட நேமா? நான் கேள்விப்பட்டதே இல்லையே?' கார்த்திகா கேட்க 'அதப் பத்தி அப்புறம் சொல்றேன்' என்று மதன் பாதியிலேயே முடித்தான்.

'வந்த வேலைய மறந்துட்டேன், கார்த்திகா, உங்ககிட்ட ஒரு ஹெல்ப் வேணும், தீப்தி உங்க ரூம்ல ஸ்டே பண்ணலாமா?' சுரேஷ் கார்த்திகாவைக் கேட்க 'அப்கோர்ஸ், பட் ரெண்டல் டெர்ம்ஸ் அண்ட் கண்டிஷன்ஸ் கயலுக்குத் தான் தெரியும், லெட் மீ கால் ஹர் டு கம் இயர்' என்று கூறி கார்த்திகா மொபைல் மூலமாகக் கயலிடம் பேசி மதன் க்யூப்பிக்களுக்கு வரவழைத்தாள். 'ஹாய் ஜீனியஸ், சூப்பர் சிங்கிங், உங்களுக்கு கம்ப்யூட்டர் மட்டும்தான் தெரியும்னு நெனச்சேன், பாட்டுலயும் அசத்துறீங்க, கொஞ்சம் விட்டிருந்தா எங்க வீட்டு நைட்டிங்கேல் கார்த்திகாவை பீட் பண்ணியிருப்பீங்க' என்று கயல் மதனிடம் கை குலுக்கி வாழ்த்துகள் தெரிவித்தாள். 'நைஸ் டு மீட் யூ' என்று மதனும் வரவேற்றான். எல்லோரும் அமர்ந்தனர், அப்போது 'கயல், திஸ் இஸ் தீப்தி, இப்ப திருவான்மியூர்ல தங்கி இருக்கா, அவ ரூம்ல இவளும் இன்னொருத்தரும் இருக்காங்க, அந்த இன்னொருத்தருக்கு கல்யாணம் ஆகப்போகுது, ரூம் ரெண்ட் வேர எக்பென்சிவ், அதுவும் இல்லாம ரெண்டு பேருதான் தங்கனுமாம், சோ, ஈதர் ஷி நீட் டு பைண்ட் அனதர் ரும்மேட் ஆர் ஷி நீட் டு மூவ் டு சம் அதர் ரூம், நான் வர்றேன்டின்னா கேட்க மாட்டேங்குரா, செருப்ப கழட்டிக் காட்றா, அதான் உங்க ரூம்ல தீப்தி ஸ்டே பண்ணிக்கலாமான்னு கார்த்திகா கிட்ட கேட்டேன்' சுரேஷ் கூற 'அப்ப கேண்டீன்ல மீட் பண்ணப்ப ராட்ஷசின்னு சொன்னது இவங்களத்தானா, ப்ளஷர் டு மீட் யு தீப்தி' என்று கயல் தீப்தியின் கைகளைக் குலுக்க தீப்தி 'ப்ளஷர் டு மீட் யூ டூ' என்று கூறி கயல் கைகளைக் குலுக்கிவிட்டு சுரேஷை முறைக்க 'ஏ, அவங்க அதுக்கு முன்னாடி சொன்ன முக்கியமான வார்த்தைய விட்டுட்டாங்கடி, அழகான ராட்ஷசின்னுதான்டி சொன்னேன்' என்று சமாளித்தான். 'கண்டிஷன் படி நாலு பேர் வரைக்கும் தங்கலாம். சோ வெல்கம் டு அவர் ரூம்' என்று கயல் தன் சம்மதத்தை வெளிப்படுத்தினாள். 'அப்ப வந்த வேலை முடிஞ்சது, அப்புறம் அந்த மூட்டை முடிச்சு தட்டு சாமான் எல்லாம் கொண்டுட்டுப் போய்க் கயல் ரூம்ல எறக்கிட்டு எனக்கு ரூம் அட்ரஸ் அனுப்பிடு, நைட் வந்து புது ரூம்ல எல்லாம் வசதியா இருக்கான்னு பாக்குறேன்' என்று சுரேஷ் கூற 'எதுக்கு அங்க வந்து மொக்க போடுறதுக்கா, பழைய ரூம் அட்ரஸ் கொடுத்துட்டு நான் இவ்வளவு நாள் பட்டதே போதும், தேவைப்படும் போது நானே சொல்றேன்' என்று தீப்தி சுரேஷிடம் முறைத்தவாறே கூறினாள். 'சாரி கய்ஸ், ஐ ஹவ் டு கோ, டீம் மீட்டிங்ல இருந்து பாதியில வந்துட்டேன், வி வில் டாக் லேட்டர், நைஸ் டு மீட் யூ ஆல்' என்று கூறிவிட்டு கயல் கிளம்ப 'எனக்கும் வொர்க் இருக்கு, ஈவ்னிங் வேற சீக்கிரமாக் கெளம்பனும், ரூம் ஷிப்ட் பண்ணனும், சீ யு அகெய்ன் மதன் அண்ணா, ஈவினிங் பாக்கலாம் கார்த்திகா அக்கா' என்று கூறி தீப்தி கிளம்ப 'ஏ இருடி நானும் வர்றேன், மச்சி ரூம்ல பாக்கலாம் டா, கார்த்திகா சீ யு அகெய்ன்' என்று கூறி சுரேஷும் கிளம்ப மீண்டும் மதனும் கார்த்திகாவும் வீடியோவிற்கு வந்த கமெண்ட்டுகளைப் படிக்க ஆரம்பித்தனர்.

'தட்ஸ் கிரேட் காம்ரேட், நெவர் தாட் ஷி இஸ் ஆல்சோ ஒன் ஆப் அஸ், btw I too use arch, சீம்ஸ் யு யூசிங் டீபால்ட் i3? வை டோன்ட் யூ RICE?' மதனின் ரிப்ளைக்கு ஒரிஜினல் கமண்ட் போட்டவர் விரைவாக ரெஸ்பான்ஸ் செய்திருந்தார். 'ஐ ஆம் நாட் குட் இன் ஆர்ட் காம்ரேட்' என்று மதன் ரிப்ளை செய்தான். 'சோ யூ கைஸ் ஆர் டாக்கிங் அபௌட் Archlinux, பட் வாட் இஸ் btw?' கார்த்திகா கேட்க 'btw எக்ஸ்பான்ஷன் பை த வே, ஆர்ச் லினக்ஸ் யூஸ் பண்றவங்க, அவங்க ஆர்ச் லினக்ஸ் யூஸ் பண்றத பெருமிதமாச் சொல்லிக்காட்ட btw I use arch அப்படின்னு சொல்லி மத்தவங்கள கடுப்பேத்துவாங்க, அதுவே போகப்போக மத்தவங்க அவங்கள கிண்டல் பண்றதுக்கும் அவங்களே அவங்கள கிண்டல் பண்ணிக்குறதுக்கும் ஒரு மீம்மா மாறிடுச்சு, அதுல இருந்து வந்ததுதான் இந்த btw, Archlinux க்கு இன்னொரு நிக் நேம்' என்று மதன் விளக்க 'அடப் பாவிகளா, இப்படியுமா கலாய்ப்பீங்க?' என்று கார்த்திகா புன்னகைத்தாள். 'ஆமா, அதென்ன நெவர் தாட் ஷி இஸ் ஆல்சோ ஒன் ஆப் அஸ், ஏன் பொண்ணுங்க லினக்ஸ யூஸ் பண்ணவே கூடாதா? திஸ் நெர்ட்ஸ் ஆர் ஆல் மிசோஜினிஸ்ட்ஸ்' கார்த்திகா மதனைப் பார்த்து கோபத்துடன் கேட்க 'ஒன் ஆப் ஆஸ் அப்படின்னா எங்கள்ள ஒருத்தின்னு அர்த்தம், காட் வை டோன்ட் திஸ் பெமினிஸ்ட்ஸ் சீ நார்மல் திங்ஸ் ஆஸ் நார்மல்' மதன் பதில் கூற 'இந்த டாப்பிக்க விட்ருவோம், இல்லன்னா தேவையில்லாம சண்ட போட்டுக்குவோம், பட் வாட் இஸ் தட் RICE?' கார்த்திகா கேட்க 'அதுவா, உங்கள் டெஸ்க்டாப்ப உங்களுக்குப் புடிச்ச மாதிரி கஸ்டமைஸ் பண்றது, ஒரு சாதாரண கார் வாங்கி அதுல வித விதமா ஸ்டிக்கர் ஒட்டி சைலன்சர் மாத்தி உள்ள இருக்குற ஸ்பேர் பார்ட்ஸ் மாத்தி அதோட பர்பாமன்ஸ் இம்ப்ருவ் பண்றாங்கள்ள, அதுக்கு பேர் Race Inspired Cosmetic Enhancements (RICE), இதே டெர்மினாலஜியதான் யுனிக்ஸ் உலகத்துல அவங்களோட டெஸ்க்டாப் அவங்களுக்குப் புடிச்சாமாதிரி கஸ்டமைஸ் பண்றதுக்கு யூஸ் பண்றாங்க, நீங்க `யுனிக்ஸ்பார்ன் <https://www.reddit.com/r/unixporn>`_ ரெடிட் கம்யூனிட்டில இருக்குற போஸ்ட்டுங்க பாத்திங்கன்னா எல்லாம் இப்படி கஸ்டமைஸ் பண்ண லினக்ஸ்/யுனிக்ஸ் டெஸ்க்டாப் ஸ்கிரீன் ஷாட் போஸ்ட்டுங்களாத்தான் இருக்கும்' என்று சொல்லி முடித்துவிட்டு அந்த யூஆர்எல் பிரவுசரில் டைப் செய்ய 'நோ, ஓப்பன் பண்ணாதீங்க, அந்த யூஆர்எல் ஒரு மாதிரி இருக்கு,ஓப்பன் பண்ணா செக்யூரிட்டி டீம்ல இருந்து எவன்னா வரப்போறான்' என்று கார்த்திகா பயந்தாள். அதற்கு மதன் 'டோண்ட் வொரி, எவனா கேட்டான்னா அவனுக்கும் ஓப்பன் பண்ணி காட்டிடலாம்' என்று கூறி அந்த யூஆர்எல் ஓப்பன் செய்தான். கார்த்திகா அந்த வெப்சைட்டை பார்த்துவிட்டு வியப்புடன் 'இஸ் திஸ் லினக்ஸ்?' என்று கேட்க 'லினக்ஸ் மட்டும் இல்ல பிஎஸ்டி, மேக்' மதன் கூற 'இவ்ளோ அழகா கஸ்டமைஸ் பண்றாங்க, நீங்க மட்டும் ஏன் உங்க டெஸ்க்டாப்ப வெறும் பிளெயின் பிளாக் அண்ட் வைட்ல வச்சிருக்கீங்க?' என்று கார்த்திகா கேட்க 'RICE பண்றது சாதாரண விஷயம் இல்லைங்க, நிறைய டைம் ஸ்பென்ட் பண்ணனும், ஒரு ஓவியம் வரையற மாதிரி, இட்ஸ் அன் ஆர்ட், ஐ ஆம் நாட் குட் இன் ஆர்ட், பிசைட்ஸ், ஐ சீ ப்யூட்டி இன் சிம்ப்ளிசிட்டி' மதன் கூற 'ஐ நோ, கருப்புத்தான் எனக்குப் புடிச்ச கலருன்னு சொன்னப்பவே நீங்க இப்படித்தான் இருப்பீங்கன்னு தோணுச்சு' கார்த்திகா புன்னகையுடன் கூறினாள்.

'எனிவே, நேரமாச்சு, சீ யூ நெக்ஸ்ட் டைம் காம்ரேட்' கார்த்திகா புன்னகையுடன் கூற 'காம்ரேட்?' மதன் வியப்புடன் பார்க்க 'பிகாஸ் ஐ ஆம் ஆல்சோ ஒன் ஆப் ஆஸ்' என்று கார்த்திகா புன்னகையுடன் கூறிவிட்டுச் செல்ல 'டேக் கேர் ஆஃப் மை சிஸ்டர் காம்ரேட்' என்று மதன் தீப்தியை நல்லபடியாக பார்த்துக் கொள் என்று கூற 'டோன்ட் வொரி, வி வில் டேக் கேர்' என்று சொன்னபடியே மதனிடம் இருந்து விடைபெற்றாள்.

**தொடரும்..**
