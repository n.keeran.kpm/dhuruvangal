.. title:: முடிவுரை

********
முடிவுரை
********

இக்கதையை நான் எழுதத் தொடங்கியபோது எனக்குக் கணினியில் தமிழில் தட்டச்சு செய்யத் தெரியாது, அதைக் கற்றுக்கொள்ளவே இக்கதையை நான் என்னுடைய கணினியில் தமிழ்99 விசைப்பலகை முறையை நிறுவி `Emacs <https://www.gnu.org/software/emacs/>`_ இல் எழுத ஆரம்பித்தேன். இக்கதையை முடிக்கும்போது என்னால் சரளமாகத் தமிழில் தட்டச்சு செய்ய முடிந்தது. எனக்கு `தமிழ்99 <https://en.wikipedia.org/wiki/Tamil_99>`_ விசைப்பலகை முறையை அறிமுகப்படுத்திய தோழர் `அன்வர் <https://twitter.com/gnuanwar>`_ அவர்களுக்கு என் நன்றியைத் தெரிவித்துக் கொள்கின்றேன். இன்னும் என்னால் பிழை இல்லாமல் தமிழில் எழுத இயலாது. இக்கதையில் பல இடங்களில் தமிழில் இருக்கும் அனைத்து வகையான பிழைகளையும் உங்களால் கண்டுபிடிக்க முடியும். ஆனாலும் இக்கதையில் நம் அன்றாட வாழ்வில் பயன்படுத்தும் பேச்சுவழக்கைப் பயன்படுத்தியுள்ளேன். ஆதலால் பிழைகளைப் பொருட்படுத்த மாட்டீர்கள் என்று நம்புகிறேன்.

இக்கதையில் இருந்த எண்ணற்ற பிழைகளைத் திருத்தம் செய்து கொடுத்த தோழர் `முத்துராமலிங்கம் கிருட்டினன் <https://twitter.com/muthu1809>`_ அவர்களுக்கு என் நன்றியைத் தெரிவித்துக் கொள்கின்றேன். இக்கதையை வெளியிடத் துணைபுரிந்த தோழர் `சீனிவாசன் <https://twitter.com/tshrinivasan>`_ அவர்களுக்கு என்றும் நான் கடமைப்பட்டுள்ளேன். அட்டைப்படம் உருவாக்கத் துணைபுரிந்த தோழர்கள் `பரமேஷ்வர் அருணாச்சலம் <https://github.com/parameshwar-A>`_ , `லெனின் குருசாமி <https://twitter.com/guru_lenin>`_ இருவருக்கும் என் மனமார்ந்த நன்றியைத் தெரிவித்துக் கொள்கிறேன்.

இக்கதை பகிர்தலை மட்டுமே நோக்கமாகக் கொண்டு எழுதப்பட்டது. தங்களுக்குத் தெரிந்த யாவருக்கும் இக்கதையைப் பகிருமாறு கேட்டுக்கொள்கின்றேன். இக்கதை பற்றிய தங்கள் கருத்துக்களை n.keeran.kpm@gmail.com என்ற மின்னஞ்சல் முகவரிக்கு அனுப்பி வைக்கவும். உங்களிடம் இருந்து வரும் கருத்துக்களைப் பார்த்தால் என் கிறுக்கல்களையும் ஒருவர் தன் நேரத்தை ஒதுக்கிப் படித்திருக்கிறார் என்று எண்ணிப் பெருமகிழ்ச்சி அடைவேன். இக்கதையில் ஏதேனும் திருத்தங்கள் இருப்பின் https://gitlab.com/n.keeran.kpm/dhuruvangal என்ற இடத்தில் தங்கள் திருத்தங்களைக் கூறலாம்.

இக்கதை மூலம் கட்டற்ற மென்பொருள் இயக்கத்தைப் பற்றிய அறிமுகம் கிடைத்து இருக்கும் என்று நம்புகிறேன். தற்போதைய கணினி மயமாக்கப்பட்ட உலகில் கட்டற்ற மென்பொருட்களின் பங்கு இன்றியமையாதது. ஆனால் இதைப் பற்றிய புரிதல் மக்களிடத்தில் கொண்டு போய்ச் சேர்க்கப்படவில்லை என்று எண்ணும்போது மனம் வருந்துகின்றது. கட்டற்ற மென்பொருள் இயக்கத்தில் எண்ணற்ற திட்டங்கள் உள்ளன, அவற்றில் தங்களை இணைத்துக்கொண்டு பங்களிக்குமாறு கேட்டுக்கொள்கின்றேன்.

.. centered:: **நன்றி**
