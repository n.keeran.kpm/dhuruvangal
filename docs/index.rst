.. title:: துருவங்கள்

*********
துருவங்கள்
*********

.. toctree::
   :caption: பொருளடக்கம்
   :name: index
   :titlesonly:

   rst/testimonials.rst
   rst/introduction.rst
   rst/episode.0.rst
   rst/episode.1.rst
   rst/episode.2.rst
   rst/episode.3.rst
   rst/episode.4.rst
   rst/episode.5.rst
   rst/episode.6.rst
   rst/episode.7.rst
   rst/episode.8.rst
   rst/episode.9.rst
   rst/episode.10.rst
   rst/episode.11.rst
   rst/episode.12.rst
   rst/episode.13.rst
   rst/episode.14.rst
   rst/episode.15.rst
   rst/conclusion.rst
