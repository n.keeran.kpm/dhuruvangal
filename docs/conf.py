# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import os


# -- Project information -----------------------------------------------------

project = 'துருவங்கள்'
copyright = '<a href="https://creativecommons.org/licenses/by/4.0/">CC-BY</a>, 2021-present, <a href="mailto:n.keeran.kpm@gmail.com">Nakeeran N</a>'
author = 'நக்கீரன்.ந'

# The full version, including alpha/beta/rc tags
release = '0.6.4'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx_markdown_builder',
    'docxbuilder'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# disable source
html_copy_source = False
html_show_sourcelink = False


# -- Options for PDF output -------------------------------------------------
latex_engine = 'xelatex'
latex_elements = {
    'releasename': 'வெளியீடு',
    'geometry': '\\usepackage[' + os.getenv('PSIZE', 'a4paper') + ']{geometry}',
    'fncychap': '',
    'fontpkg': r'''
\setmainfont{Marutham-Serif}[
    Path = ../../fonts/Marutham/,
    BoldFont = *-B,
    ItalicFont = *-I,
    BoldItalicFont = *-BI
]
\setsansfont{Marutham-Sans}[
    Path = ../../fonts/Marutham/,
    BoldFont = *-B,
    ItalicFont = *-I,
    BoldItalicFont = *-BI
]
\setmonofont{Marutham-Mono}[
    Path = ../../fonts/Marutham/,
    BoldFont = *-B,
    ItalicFont = *-I,
    BoldItalicFont = *-BI
]
    ''',
    'preamble': r'''
    \usepackage{eso-pic,graphicx}
    \titleformat{\chapter}[hang]{}{}{0pt}{\Huge}
    \renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
    ''',
    'maketitle': r'''
    \vspace*{\fill}
    \AddToShipoutPictureFG*{\includegraphics[width=\paperwidth,height=\paperheight]{../../_static/cover.png}}
    \clearpage
    \vspace*{\fill}
    \hspace*{\fill}
        {\Huge துருவங்கள்}
    \hspace*{\fill}
    \newline
    \hspace*{\fill}
        \texttt{{\Large 11 = 10 | 01}}
    \hspace*{\fill}
    \newline
    \newline
    \hspace*{\fill}
        {\Large நக்கீரன்.ந}
    \hspace*{\fill}
    \newline
    \newline
    \hspace*{\fill}
        பிழை திருத்தம்
    \hspace*{\fill}
    \newline
    \hspace*{\fill}
        முத்துராமலிங்கம் கிருட்டினன்
    \hspace*{\fill}
    \newline
    \hspace*{\fill}
        \sphinxhref{mailto:muthu1809@gmail.com}{muthu1809@gmail.com}
    \hspace*{\fill}
    \newline
    \newline
    \hspace*{\fill}
        அட்டைப்படம்
    \hspace*{\fill}
    \newline
    \hspace*{\fill}
        பரமேஷ்வர் அருணாச்சலம்
    \hspace*{\fill}
    \newline
    \hspace*{\fill}
        \sphinxhref{mailto:stark20236@gmail.com}{stark20236@gmail.com}
    \hspace*{\fill}
    \newline
    \hspace*{\fill}
        லெனின் குருசாமி
    \hspace*{\fill}
    \newline
    \hspace*{\fill}
        \sphinxhref{mailto:guruleninn@gmail.com}{guruleninn@gmail.com}
    \hspace*{\fill}
    \newline
    \newline
    \hspace*{\fill}
        வெளியீட்டாளர்
    \hspace*{\fill}
    \newline
    \hspace*{\fill}
        த.சீனிவாசன், கணியம் அறக்கட்டளை, கிழக்கு தாம்பரம், சென்னை
    \hspace*{\fill}
    \newline
    \hspace*{\fill}
        \sphinxhref{mailto:tshrinivasan@gmail.com}{tshrinivasan@gmail.com}
    \hspace*{\fill}
    \newline
    \hspace*{\fill}
        9841795468
    \hspace*{\fill}
    \newline
    \vspace*{\fill}
    \newline
    \hspace*{\fill}
        © \sphinxhref{https://creativecommons.org/licenses/by-sa/4.0/}{CC-BY-SA}, 2021-present, \sphinxhref{mailto:n.keeran.kpm@gmail.com}{Nakeeran N}
    \hspace*{\fill}
    \newline
    \hspace*{\fill}
        Publisher - \sphinxhref{https://freetamilebooks.com}{https://freetamilebooks.com}
    \hspace*{\fill}
    \newline
    \hspace*{\fill}
        \sphinxhref{http://www.kaniyam.com/foundation}{Kaniyam Foundation}
    \hspace*{\fill}
    \clearpage
    '''
}

# -- Options for EPUB output -------------------------------------------------
epub_cover = ('_static/cover.png', 'epub-cover.html')
epub_pre_files = [('_static/frontpage.xhtml', 'முகப்பு')]

# -- Options for DOCX output -------------------------------------------------
docx_documents = [
    ('index',
     'sphinx.docx',
     {
         'title': project,
         'creator': author
     },
     True)
]
