# Generating custom tamil Fonts with all unicode glyphs from freefonts
#
# why?
#
# * xelatex dont handle multi language documents by default
# * we have to use ucharclasses for xelatex for multilanguage documents
# * but ucharclasses sucks
# * so I have to create my own font combining my favourite tamil font
#   with all the glyphs from freefonts
#
# based on https://fontforge.org/docs/scripting/python.html#trivial-example
#
# $ fontforge -lang py -script gen.py <fontname> <regular.ttf> <bold.ttf> <italic.ttf> <bolditalic.ttf>
#

from pathlib import Path
import sys

freefonts = {
    'R' : [
        ('/usr/share/fonts/opentype/freefont/FreeSerif.otf', 'Serif'),
        ('/usr/share/fonts/opentype/freefont/FreeSans.otf', 'Sans'),
        ('/usr/share/fonts/opentype/freefont/FreeMono.otf', 'Mono')
    ],
    'B' : [
        ('/usr/share/fonts/opentype/freefont/FreeSerifBold.otf', 'Serif'),
        ('/usr/share/fonts/opentype/freefont/FreeSansBold.otf', 'Sans'),
        ('/usr/share/fonts/opentype/freefont/FreeMonoBold.otf', 'Mono')
    ],
    'I' : [
        ('/usr/share/fonts/opentype/freefont/FreeSerifItalic.otf', 'Serif'),
        ('/usr/share/fonts/opentype/freefont/FreeSansOblique.otf', 'Sans'),
        ('/usr/share/fonts/opentype/freefont/FreeMonoOblique.otf', 'Mono')
    ],
    'BI' : [
        ('/usr/share/fonts/opentype/freefont/FreeSerifBoldItalic.otf', 'Serif'),
        ('/usr/share/fonts/opentype/freefont/FreeSansBoldOblique.otf', 'Sans'),
        ('/usr/share/fonts/opentype/freefont/FreeMonoBoldOblique.otf', 'Mono')
    ]
}

def merge(f0, f1, g0):
    try:
        f1.selection.select(g0)
        f1.copy()
        f0.selection.select(g0)
        f0.paste()
    except Exception as error:
        print(g0, g0.encoding, error)
        g1 = f0.createInterpolatedGlyph(g0, g0, 0)
        if g1:
            print("%s added to %s as %s"%(g0, f0, g1))
        else:
            print("something went wrong when creating glyph for %s"%(g0))
    
def main():
    if len(sys.argv) < 6:
        print("[usage] %s <fontname> <regular.ttf> <bold.ttf> <italic.ttf> <bolditalic.ttf>"%(sys.argv[0]))
        exit(1)

    fontname = sys.argv[1]
    font = [(sys.argv[2], 'R'), (sys.argv[3], 'B'), (sys.argv[4], 'I'), (sys.argv[5], 'BI')]
    for t0 in font:
        f0 = fontforge.open(t0[0])
        style = t0[1]
        for t1  in freefonts[style]:
            f1 = fontforge.open(t1[0])
            family = t1[1]
            f0.encoding = f1.encoding
            f0.em = f1.em
            f0.weight = f1.weight
            for g0 in f1.glyphs():
                if g0.encoding not in f0:
                    merge(f0, f1, g0)
                else:
                    if g0.encoding >= 0x0b80 and g0.encoding <= 0x0bff: # make sure tamil glyphs not replaced
                        g1 = f0[g0.encoding]
                        print("%s already available as %s"%(g0, g1))
                    else:
                        merge(f0, f1, g0)
            f0.fontname = fontname + '-' + family + ( '-' + style if style != 'R' else '')
            f0.generate(f0.fontname + '.ttf')

main()
